# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# A C <ana@shiftout.net>, 2019
# Alex Alex <alexandruvuia@gmail.com>, 2016
# Alex Alex <alexandruvuia@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-06 09:02+0200\n"
"PO-Revision-Date: 2019-01-10 14:33+0000\n"
"Last-Translator: A C <ana@shiftout.net>\n"
"Language-Team: Romanian (http://www.transifex.com/otf/torproject/language/"
"ro/)\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?"
"2:1));\n"

#: ../onioncircuits:84
msgid "You are not connected to Tor yet..."
msgstr "Încă nu eşti conectat la Tor..."

#: ../onioncircuits:98
msgid "Onion Circuits"
msgstr "Onion Circuits"

#: ../onioncircuits:128
msgid "Circuit"
msgstr "Circuit"

#: ../onioncircuits:129
msgid "Status"
msgstr "Stare"

#: ../onioncircuits:146
msgid "Click on a circuit for more detail about its Tor relays."
msgstr ""
"Apasă pe un circuit pentru mai multe detalii despre releurile Tor ale "
"acestuia."

#: ../onioncircuits:236
msgid "The connection to Tor was lost..."
msgstr "Conexiunea cu Tor s-a pierdut..."

#: ../onioncircuits:332
msgid "..."
msgstr "..."

#: ../onioncircuits:358
#, c-format
msgid "%s: %s"
msgstr "%s: %s"

#: ../onioncircuits:569
msgid "GeoIP database unavailable. No country information will be displayed."
msgstr ""
"Baza de date GeoIP este indisponibilă. Nu se arată nicio informaţie despre "
"ţări."

#: ../onioncircuits:599
#, c-format
msgid "%s (%s)"
msgstr "%s (%s)"

#: ../onioncircuits:603
#, c-format
msgid "%.2f Mb/s"
msgstr "%.2f Mb/s"

#: ../onioncircuits:605 ../onioncircuits:606
msgid "Unknown"
msgstr "Necunoscut"

#: ../onioncircuits:619
msgid "Fingerprint:"
msgstr "Amprentă: "

#: ../onioncircuits:620
msgid "IP:"
msgstr "IP:"

#: ../onioncircuits:621
msgid "Bandwidth:"
msgstr "Lăţime de bandă:"

#~ msgid "Published:"
#~ msgstr "Publicat:"

Coding style
============

Please follow PEP-8 "Style Guide for Python Code" [1] and be consistent with
current code when proposing patches. Explicit variables names are preferred,
even if they are longer.

[1] https://www.python.org/dev/peps/pep-0008/


Release
=======

1. Pull last commits:

        git checkout master
        git pull

2. Update translations:

        ~/path/to/tails/git/import-translations && \
        rm -f po/*.po~ && \
        git add po/*.po po/*.pot && \
        git commit po -m 'Update POT and PO files.'

3. Edit `setup.py` to increment the `version=` field to `$VERSION`

4. Commit the version bump, tag the release, and push:

        git add setup.py && \
        git commit -m "Bump version to ${VERSION:?}" && \
        git tag -m "Release version $VERSION" -s "${VERSION:?}" && \
        git push --follow-tags origin/master

Onion Circuits - a GTK application to display Tor circuits and streams
Copyright (C) 2016  Tails developers

Dependencies
============

Onion Circuits depends on python3 (>= 3.4.2), python3-gi (>= 3.14.0), python3-stem
(>= 1.2.2), Gtk GIR (>= 3.14), GLib GIR (>= 1.42).

It recommends pycountry (>= 1.8).

Onion Circuits also build-depends on python3-distutils-extra.

On Debian system (>= 8.0), these can be installed with:

    apt-get install python3-gi python3-stem python3-pycountry \
         python3-distutils-extra gir1.2-gtk-3.0 gir1.2-glib-2.0

Installation
============

    ./setup.py build
    sudo ./setup.py install

Apparmor
========

Apparmor profile is included in apparmor/ directory, but not installed by
setup.py. The user or downstream packager is expected to take care of its
installation according to their system.

Feedback
========

Please submit your bug reports to
https://gitlab.tails.boum.org/tails/tails/-/issues
with the `C:Onion Circuits` label:

https://gitlab.tails.boum.org/tails/tails/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=C%3AOnion%20Circuits
